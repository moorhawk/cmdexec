#include <exception>
#include <iostream>

#include <cmdexec.h>

using namespace cmdexec;
using namespace std;

int main(int argc, char **argv) {

    if (argc < 2) {
        cerr << "Not enough arguments" << endl;
    }

    CmdExec cmd(argv[1]);

    for (int i = 2; i < argc; i++) {
        cmd.addArg(argv[i]);
    }

    try {
        int res = cmd.run();

        if (cmd.exited()) {
            cout << "Command exited with status code: " << res << endl;
        } else if (cmd.wasSignaled()) {
            cout << "Command was interrupted by signal: "
                 << cmd.terminationSignal() << endl;
        }

    } catch (const std::exception &e) {
        cout << e.what() << endl;
    }

    return 0;
}
