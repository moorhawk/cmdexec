#include "cmdexec.h"

#include <cerrno>
#include <cstring>
#include <exception>
#include <sstream>
#include <system_error>

#include <fcntl.h>
#include <sys/wait.h>
#include <unistd.h>

namespace cmdexec {

static const unsigned VERSION_MAJOR = GIT_MAJOR;
static const unsigned VERSION_MINOR = GIT_MINOR;
static const unsigned VERSION_PATCH = GIT_PATCH;

Version get_version() {
    return Version{VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH};
}

char default_path[] = "PATH=/sbin:/bin:/usr/sbin:/usr/bin";
#define DEFAULT_BUFFER_SIZE 8096
#define RETRY_MAX 5

void CmdExec::resolveCommandPath(std::string cmd_name) {

    /* If the path is relative and exists it is done */
    if (std::filesystem::exists(std::filesystem::path(cmd_name))) {
        this->command = cmd_name;
        return;
    }

    for (auto &dir : this->lookup_path) {
        std::filesystem::path bin_path = dir;
        bin_path /= cmd_name;

        if (std::filesystem::exists(bin_path)) {
            this->command = bin_path;
            this->options[0] = bin_path;
            break;
        }
    }

    if (this->command.empty()) {
        throw std::runtime_error("executable not found in path");
    }
}

CmdExec::CmdExec(std::string cmd) {

    std::string option;
    std::istringstream iss(cmd);
    std::string cmd_name;

    this->outputToPipe = false;
    this->backgroundTask = false;
    int res = pipe(this->contentPipes);
    if (-1 == res) {
        throw std::runtime_error("Pipe creation Failed");
    }

    /* Create pipe with CLOEXEC option. This will cause the pipe to be close
     * once exec syscall succeeds. If it fails it will remain open for
     * communication.
     */
    res = pipe2(this->statusPipes, O_CLOEXEC);
    if (-1 == res) {
        throw std::runtime_error("Pipe creation Failed");
    }

    iss >> this->cmd_name;

    /* argv[0] == name of the command */
    this->options.push_back(this->cmd_name);

    while (iss >> option) {
        this->options.push_back(option);
    }

    this->lookup_path.push_back(std::string("/sbin"));
    this->lookup_path.push_back(std::string("/bin"));
    this->lookup_path.push_back(std::string("/usr/sbin"));
    this->lookup_path.push_back(std::string("/usr/bin"));

    this->envp.push_back(default_path);
    this->envp.push_back(nullptr);
}

CmdExec::~CmdExec() {}

void CmdExec::addArg(std::string arg) { this->options.push_back(arg); }

int CmdExec::waitForCommand(pid_t pid) {
    pid_t childpid;

    childpid = waitpid(pid, &this->status, 0);

    if (childpid < 0) {
        throw std::runtime_error("Wait Failed");
    }

    return WEXITSTATUS(this->status);
}

void CmdExec::execute() {
    char out;
    ssize_t n;
    int i = 0;
    std::vector<const char *> argv;

    for (auto const &arg : this->options) {
        argv.push_back(arg.c_str());
    }
    argv.push_back(nullptr);

    if (this->outputToPipe) {
        this->redirectStdoutToPipe();
    }

    close(this->statusPipes[INPUT_PIPE]);

    /* output is ignored as it doesnt matter */
    execve(this->command.c_str(),
           (char *const *)(&argv[0]),
           static_cast<char *const *>(&this->envp[0]));

    /* if execvpe succeeds we will never reach this point. in case of a failure
     * we have to notify the parent process.
     */
    out = errno;
    do {
        n = write(this->statusPipes[OUTPUT_PIPE], &out, 1);
        i++;
    } while (n < 1 || i > RETRY_MAX);

    close(this->statusPipes[OUTPUT_PIPE]);
    /* exit cleanly */
    exit(errno);
}

int CmdExec::run() {
    resolveCommandPath(this->cmd_name);

    pid_t pid = fork();
    int res = 0;

    if (0 == pid) {
        this->execute();
    } else if (pid > 0) {
        /* We will not need to write to the child */
        close(this->statusPipes[OUTPUT_PIPE]);
        close(this->contentPipes[OUTPUT_PIPE]);
        this->command_pid = pid;
        if (!this->backgroundTask) {
            res = this->waitForCommand(pid);

            char v;
            int n = read(this->statusPipes[INPUT_PIPE], &v, 1);

            close(this->statusPipes[INPUT_PIPE]);
            if (n > 0) {
                throw std::system_error(n, std::generic_category());
            }
        }
    } else {
        throw std::runtime_error("Could not fork");
    }

    return res;
}

int CmdExec::join() { return this->waitForCommand(this->command_pid); }

void CmdExec::redirectStdoutToPipe() {
    int res = dup2(this->contentPipes[OUTPUT_PIPE], STDOUT_FILENO);
    if (-1 == res) {
        throw std::runtime_error("Stdout dup2 failed");
    }
}

void CmdExec::setBackgroundTask(bool state) { this->backgroundTask = state; }
void CmdExec::setOutputToPipe(bool state) { this->outputToPipe = state; }

std::string CmdExec::getCommandOutput() {
    std::string output;
    char buffer[DEFAULT_BUFFER_SIZE];
    int nbytes;

    do {
        nbytes =
            read(this->contentPipes[INPUT_PIPE], buffer, DEFAULT_BUFFER_SIZE);
        if (nbytes > 0) {
            output.append(buffer, nbytes);
        }
    } while (nbytes == DEFAULT_BUFFER_SIZE);

    close(this->contentPipes[INPUT_PIPE]);

    return output;
}

bool CmdExec::exited() { return WIFEXITED(this->status); }

bool CmdExec::wasSignaled() { return WIFSIGNALED(this->status); }

int CmdExec::terminationSignal() { return WTERMSIG(this->status); }

void CmdExec::printAll() {
    std::cout << "cmd " << this->command << std::endl;

    for (auto const &c : this->options) {
        std::cout << "arg: " << c << std::endl;
    }
}

} // namespace cmdexec
