#include <iostream>

int main(int argc, char **argv) {
    int i;

    if (argc < 2) {
        return 1;
    }

    std::cout << "ptr " << argv << std::endl;
    for (i = 0; i < argc; i++) {
        argv[i][0] = 'X';
        std::cout << "argv " << i << " " << argv[i] << std::endl;
    }

    return 0;
}
