#include <gtest/gtest.h>

#include "cmdexec.h"

using namespace cmdexec;

TEST(CmdExec, Simple) {
    CmdExec cmd(std::string("echo one two three"));

    int res = cmd.run();

    EXPECT_EQ(res, 0);
}

TEST(CmdExec, Redirection) {
    CmdExec cmd(std::string("echo one two three"));

    cmd.setOutputToPipe(true);
    int res = cmd.run();

    std::string out = cmd.getCommandOutput();

    EXPECT_EQ(res, 0);
    EXPECT_EQ(out, std::string("one two three\n"));
}

TEST(CmdExec, Background) {
    CmdExec cmd(std::string("echo one two three"));

    cmd.setOutputToPipe(true);
    cmd.setBackgroundTask(true);
    int res = cmd.run();

    cmd.join();
    std::string out = cmd.getCommandOutput();

    EXPECT_EQ(res, 0);
    EXPECT_EQ(out, std::string("one two three\n"));
}

TEST(CmdExec, AddArg) {
    CmdExec cmd(std::string("cat"));
    cmd.addArg(std::string("./data/file with spaces in name.txt"));

    EXPECT_EQ(cmd.run(), 0);
}

TEST(CmdExec, InexistantCommand) {
    CmdExec cmd(std::string("this_command_does_not_exist"));

    EXPECT_THROW(cmd.run(), std::runtime_error);
}

TEST(CmdExec, NoExecBitSet) {
    CmdExec cmd(std::string("./scripts/noexec.sh"));

    EXPECT_THROW(cmd.run(), std::system_error);
}

TEST(CmdExec, ReturnVal) {
    CmdExec cmd(std::string("./scripts/exit_with_val.sh 42"));

    int res = cmd.run();
    EXPECT_EQ(res, 42);
}

TEST(CmdExec, InterruptedProgram) {
    CmdExec cmd(std::string("./scripts/kill_myself.sh 9"));

    cmd.run();
    EXPECT_FALSE(cmd.exited());
    EXPECT_TRUE(cmd.wasSignaled());
    EXPECT_EQ(9, cmd.terminationSignal());
}

TEST(CmdExec, Version) {
    Version v = get_version();
    EXPECT_EQ(GIT_MAJOR, v.MAJOR);
    EXPECT_EQ(GIT_MINOR, v.MINOR);
    EXPECT_EQ(GIT_PATCH, v.PATCH);
}
