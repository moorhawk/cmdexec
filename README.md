# Introduction

The goal of this project is to provide a simple abstraction to an exec syscall.
While you have [system](https://man7.org/linux/man-pages/man3/system.3.html), it
can be dangerous in the wrong hands. This project aims to provide an abstraction
that is as safe as possible by default.

# Dependencies

To build the library you will require make, g++ and ar. Note the library also
builds with clang so you can switch out g++ for clang++.

For the reformating you will require the presence of clang-format.

To build the test cases you will need to install gtest as well.

If you wish to run the memory leak tests valgrind is required.

To generate the documentation you will need to install doxygen.

# How to build

```
make
```

# How to test

To run the normal test cases:

```
make runtest
```

To check for memory leaks:

```
make memcheck
```

# How to fix the style issues

```
make fixstyle
```

# How to generate documentation

```
make doc
```

# Build examples

```
make examples
```
