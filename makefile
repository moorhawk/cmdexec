define add_define
-D$1=$($1)
endef

INC = include
BUILD = build
SRCDIR = src
EXAMPLESDIR = examples

SRC = $(wildcard $(SRCDIR)/*.cpp)
EXAMPLESSRC = $(wildcard $(EXAMPLESDIR)/*.cpp)

OBJ = $(SRC:.cpp=.o)

EXAMPLESBIN = $(EXAMPLESSRC:.cpp=.out)
LIB = libcmdexec.a

TSRC = $(wildcard test/*.cpp)
TPROG = $(TSRC:.cpp=.out)

DOC = doc

# Split Tag into three variables
TAG = $(shell git tag --sort=-version:refname)
GIT_MAJOR = $(firstword $(subst ., ,$(TAG)))
GIT_MINOR = $(word 2, $(subst ., ,$(TAG)))
GIT_PATCH = $(word 3, $(subst ., ,$(TAG)))

DEFINES += $(call add_define,GIT_MAJOR)
DEFINES += $(call add_define,GIT_MINOR)
DEFINES += $(call add_define,GIT_PATCH)

CXXFLAGS = -std=c++17 -W -Wall -Wextra -Werror -pedantic -O2 $(DEFINES)

.PHONY: all test runtest clean fixstyle memcheck examples

all: $(LIB)

$(LIB): $(OBJ)
	$(AR) rvs $@ $^

test: $(LIB) $(TPROG)
examples: $(LIB) $(EXAMPLESBIN)

%.o: %.cpp
	$(CXX) -I $(INC) $(CXXFLAGS) -c -o $@ $^

%.out: %.cpp $(LIB)
	$(CXX) -I $(INC) $(CXXFLAGS) -L . -o $@ $^ -lcmdexec -lpthread -lgtest -lgtest_main

clean:
	rm -rf $(OBJ) $(LIB) $(TPROG) $(DOC) $(EXAMPLESBIN)

fixstyle:
	clang-format -i $(INC)/*.h $(SRC) $(TSRC) $(EXAMPLESSRC)

runtest: test
	./test/runtest.out

memcheck: test
	valgrind --leak-check=full ./$(TPROG)

doc: $(INC) $(SRC)
	doxygen doxygenfile
