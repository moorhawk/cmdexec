#ifndef CMD_EXEC_H
#define CMD_EXEC_H

#include <filesystem>
#include <iostream>
#include <vector>

#include <sys/types.h>

namespace cmdexec {

typedef struct _version {
    const unsigned MAJOR;
    const unsigned MINOR;
    const unsigned PATCH;
} Version;

/**
 * Get library version
 *
 * @return Version struct containing major.minor.patch semver
 */
Version get_version();

typedef enum {
    INPUT_PIPE = 0,
    OUTPUT_PIPE = 1,
    MAX_PIPE = 2,
} PipeChannels;

/**
 * @class CmdExec
 *
 * The class has the goal to provide an alternative to system() command by
 * allowing c++ programs to call subcommands in a safe manner.
 */
class CmdExec {
  private:
    pid_t command_pid;

    std::string cmd_name;
    std::filesystem::path command;

    std::vector<std::string> options;
    std::vector<std::string> lookup_path;
    std::vector<char *> envp;

    bool outputToPipe;
    bool backgroundTask;
    int contentPipes[MAX_PIPE];
    int statusPipes[MAX_PIPE];
    int status;

    /**
     * Wait on pid
     *
     * @return int exit status
     */
    int waitForCommand(pid_t pid);

    /**
     * Internal call to execve
     *
     */
    void execute();

    /**
     * Redirect output of the command to a pipe
     *
     * This is required if you want to retrieve the output of the command later.
     */
    void redirectStdoutToPipe();

    /**
     * Search for command in default paths
     *
     * @throws runtime_error if no command is found.
     */
    void resolveCommandPath(std::string cmd_name);

  public:
    /**
     * Create command based on string
     *
     * @param[in] cmd space separate command (e.g CMD ARG1 ARG2)
     *
     * @note The elements in the string will be split by space. The first
     * element will be the name of the progam that we will execute. During exec
     * call the class will hand over all subsequent elements as arguments to the
     * command.
     */
    CmdExec(std::string cmd);

    /**
     * CmdExec destructor
     *
     * This will clean up any allocated arrays for argv.
     *
     */
    ~CmdExec();

    /**
     * Add one argument to the list
     *
     */
    void addArg(std::string arg);

    /**
     * Run command
     *
     * @return exit value from the command (invalid if wait is false)
     *
     * @note function will wait by default unless @setBackgroundTask is called.
     */
    int run();

    /**
     * Wait on end of the command
     *
     * @return exit value from command
     *
     */
    int join();

    /**
     * Enable/disable command output to pipe
     *
     * @param[in] if state is true output will be redirected to pipe otherwise
     * output is not captured. No redirection is the default if nothing is done.
     */
    void setOutputToPipe(bool state);

    /**
     * Enable/disable waiting for command to finish
     *
     * @param[in] if state is true the class will not wait for the end of the
     * command. This can be useful to let the command run in background while
     * doing other stuff. If the value is false then the command will be waited
     * on.
     */
    void setBackgroundTask(bool state);

    /**
     * Get output of the executed command
     *
     * @return string containing complete stdout output
     *
     * @note this will only work if the stdout redirection has been enabled.
     */
    std::string getCommandOutput();

    /**
     * Indicates if the process exited correctly
     *
     * @return bool true if the process exited normally and false otherwise
     *
     * @note Normally means through the exit syscall or a return. If the process
     * gets interrupted with a termination signal then it could not exit
     * normally.
     */
    bool exited();

    /**
     * Indicates if the process was killed through a signal
     *
     * @return bool true if the process was interrupted by a signal
     *
     */
    bool wasSignaled();

    /**
     * Get the signal used to terminate the process.
     *
     * @return int return the integer value representing the received signal.
     *
     */
    int terminationSignal();

    /**
     * Debug function printing internals of the class
     *
     */
    void printAll();
};

} // namespace cmdexec
#endif
